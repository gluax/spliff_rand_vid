import video_data from './../videos/video_data.js';

Object.defineProperty(Array.prototype, 'shuffle', {
	value: function() {
			for (let i = this.length - 1; i > 0; i--) {
					const j = Math.floor(Math.random() * (i + 1));
					[this[i], this[j]] = [this[j], this[i]];
			}
			return this;
	}
});

video_data.shuffle();
const video_player = document.getElementById("video_player");
const video_src = document.getElementById("video_src");

function pick_video() {
	if (video_data.length === 0) return;
	
	const current_video_index = Math.floor((Math.random() * video_data.length));
	const current_video = video_data[current_video_index];
	video_data.splice(current_video_index, 1);

	video_src.src = `${current_video.path}.${current_video.extension}`;
	video_src.type = `video/${current_video.extension}`;
	video_player.load();
}

window.onload = () => {
	pick_video();
};

video_player.addEventListener('ended', () => {
	pick_video();
});

video_player.addEventListener('mouseenter', () => {
	video_player.setAttribute('controls', 'true');
});

video_player.addEventListener('mouseleave', () => {
	video_player.removeAttribute("controls");
});